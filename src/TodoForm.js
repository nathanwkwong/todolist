import React from "react";
import { useState } from "react";

const TodoForm = ({ saveTodo }) => {
  const [value, setValue] = useState("");

  const onChange = (event) => {
    setValue(event.target.value);
  };

  const reset = () => setValue("");

  return (
    <form
      onSubmit={(event) => {
        event.preventDefault();

        saveTodo(value);
        reset();
      }}
    >
      <input placeholder="Add todo" onChange={onChange} value={value} />
    </form>
  );
};

export default TodoForm;
