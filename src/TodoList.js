import React from 'react';
import List from '@material-ui/core/List';

const TodoList = ({ todos, deleteTodo }) => (
  <List>
    {todos.map((todo, index) => (
      <div className="todo-item" key={index.toString()} >
          {todo}
          <div
            className="btn-delete"
            onClick={() => {
              deleteTodo(index);
            }}
          >
            delete
          </div>
      </div>
    ))}
  </List>
);

export default TodoList;
